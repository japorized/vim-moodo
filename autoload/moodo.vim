" Configurables
let s:moodo_modified_indicator = get(g:, 'moodo_modified_indicator', '[+]')
let s:moodo_tabline_closestr = get(g:, 'moodo_tabline_closestr', 'X')
let s:moodo_left = get(g:, 'moodo_left', '')
let s:moodo_mid = get(g:, 'moodo_mid', "\ %r%h\ %.40f%(\ %q%{&modified?moodo#MoodoModifiedIndicator():''}%)\ ")
let s:moodo_right = get(g:, 'moodo_right', "%(%{moodo#MoodoFT()}%) %l/%L\ %2c\ ")
let s:moodo_fticons = get(g:, 'moodo_fticons', {
  \ })
let s:moodo_colors = get(g:, 'moodo_colors', {
  \ 'n'      : '%#StatusLine#',
  \ 'no'     : '%#MoodoNormal#',
  \ 'v'      : '%#MoodoVisual#',
  \ 'V'      : '%#MoodoVisual#',
  \ "\<C-V>" : '%#MoodoVisual#',
  \ 's'      : '%#MoodoSelect#',
  \ 'S'      : '%#MoodoSelect#',
  \ "\<C-S>" : '%#MoodoSelect#',
  \ 'i'      : '%#MoodoInsert#',
  \ 'ic'     : '%#MoodoInsert#',
  \ 'ix'     : '%#MoodoInsert#',
  \ 'R'      : '%#MoodoReplace#',
  \ 'Rc'     : '%#MoodoReplace#',
  \ 'Rv'     : '%#MoodoReplace#',
  \ 'Rx'     : '%#MoodoReplace#',
  \ 'c'      : '%#MoodoCommand#',
  \ 'cv'     : '%#MoodoCommand#',
  \ 'ce'     : '%#MoodoCommand#',
  \ 'r'      : '%#MoodoMore#',
  \ 'rm'     : '%#MoodoMore#',
  \ '!'      : '%#MoodoCommand#',
  \ 't'      : '%#MoodoTerminal#'
  \ })

let s:moodo_modes = get(g:, 'moodo_modes', {
  \ 'n'      : 'Normal',
  \ 'no'     : 'No-OP',
  \ 'v'      : 'Visual',
  \ 'V'      : 'Visual',
  \ "\<C-V>" : 'V-Block',
  \ 's'      : 'Select',
  \ 'S'      : 'Select',
  \ "\<C-S>" : 'S-Block',
  \ 'i'      : 'Insert',
  \ 'ic'     : 'Insert',
  \ 'ix'     : 'Insert',
  \ 'R'      : 'Replace',
  \ 'Rc'     : 'Replace',
  \ 'Rv'     : 'Replace',
  \ 'Rx'     : 'Replace',
  \ 'c'      : 'Command',
  \ 'cv'     : 'Vim-Ex',
  \ 'ce'     : 'Ex',
  \ 'r'      : 'Prompt',
  \ 'rm'     : 'Prompt',
  \ '!'      : 'Shell',
  \ 't'      : 'Terminal'
  \ })

" Statusline Custom Highlight Groups
hi MoodoNormal      ctermbg=2    ctermfg=0   cterm=NONE    guibg=#70ef9b   guifg=#2d2d2d   gui=NONE
hi MoodoInsert      ctermbg=4    ctermfg=0   cterm=NONE    guibg=#70c3ff   guifg=#2d2d2d   gui=NONE
hi MoodoVisual      ctermbg=6    ctermfg=0   cterm=NONE    guibg=#34a9d1   guifg=#2d2d2d   gui=NONE
hi MoodoSelect      ctermbg=3    ctermfg=0   cterm=NONE    guibg=#ead637   guifg=#2d2d2d   gui=NONE
hi MoodoReplace     ctermbg=1    ctermfg=0   cterm=NONE    guibg=#fc3737   guifg=#2d2d2d   gui=NONE
hi MoodoCommand     ctermbg=5    ctermfg=0   cterm=NONE    guibg=#8d66ff   guifg=#2d2d2d   gui=NONE
hi MoodoMore        ctermbg=5    ctermfg=0   cterm=NONE    guibg=#8d66ff   guifg=#2d2d2d   gui=NONE
hi MoodoTerminal    ctermbg=9    ctermfg=0   cterm=NONE    guibg=#cf2e2e   guifg=#2d2d2d   gui=NONE
hi MoodoTabLine     ctermbg=NONE ctermfg=15  cterm=NONE    guibg=#2d2d2d   guifg=#ededed   gui=NONE
hi MoodoTabLineSel  ctermbg=2    ctermfg=0   cterm=NONE    guibg=#70ef9b   guifg=#2d2d2d   gui=NONE
hi MoodoTabLineFill ctermbg=NONE ctermfg=15  cterm=NONE    guibg=#2d2d2d   guifg=#ededed   gui=NONE

" Functions
function! moodo#MoodoSetStatusline() abort
  set statusline=%!moodo#Moodo()
endfunction

function! moodo#Moodo() abort
  let s:mode = mode(1)
  let moodo_mode = moodo#MoodoMode(s:mode)
  let statuscolor = moodo#MoodoColor(s:mode)
  return statuscolor . '  ' . moodo_mode . ' ' . s:moodo_left . "  %#Normal# " . s:moodo_mid . "%= " . statuscolor . s:moodo_right . '%(%{MoodoIndicators()}%)'
endfunction

function! moodo#MoodoColor(mode)
  return get(s:moodo_colors, a:mode, '%#ErrorMsg#')
endfunction

function! moodo#MoodoMode(mode)
  return get(s:moodo_modes, a:mode, a:mode)
endfunction

function! moodo#MoodoFT() abort
  let s = get(s:moodo_fticons, &filetype, &filetype)
  return s == '' ? '' : '  ' . s . '  |'
endfunction

function! moodo#MoodoModifiedIndicator() abort
  return s:moodo_modified_indicator
endfunction

" Tabline
set tabline=%!moodo#MoodoTabline()
function! moodo#MoodoTabline()
  let str = ''
  for i in range(tabpagenr('$'))
    " select the highlighting
    if i + 1 == tabpagenr()
      let str .= '%#MoodoTabLineSel#'
    else
      let str .= '%#MoodoTabLine#'
    endif

    " set the tab page number (for mouse clicks)
    let str .= '%' . (i + 1) . 'T'

    let str .= ' %.20{moodo#MoodoTabLabel(' . (i + 1) . ')} '
  endfor

  " after the last tab fill with TabLineFill and reset tab page nr
  let str .= '%#MoodoTabLineFill#%T'

  " right-align the label to close the current tab page
  if tabpagenr('$') > 1
    let str .= '%=%#MoodoTabLineSel#%999X ' . s:moodo_tabline_closestr . ' '
  endif

  return str
endfunction

function! moodo#MoodoTabLabel(n)
  let buflist = tabpagebuflist(a:n)
  let winnr = tabpagewinnr(a:n)
  let fin = bufname(buflist[winnr - 1]) . (getbufvar(buflist[winnr - 1], "&mod")?' ' . s:moodo_modified_indicator : '')
  if fin == ''
    return ' ... '
  else
    return fin
  endif
endfunction

