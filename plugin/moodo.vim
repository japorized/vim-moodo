" Statusline
augroup moodo
  autocmd!
  autocmd WinEnter,BufWinEnter,FileType,SessionLoadPost * call moodo#MoodoSetStatusline()
augroup end

if !exists("*MoodoIndicators")
  function! MoodoIndicators() abort
    return ''
  endfunction
endif
