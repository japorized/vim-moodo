# vim-moodo

vim-moodo is a barebones statusline that provides sane defaults, and a sane framework to creating your own statusline (and, for now, basic configuration over tabline).

<div align="center">
	<img src="./preview.png" alt="Logo">
</div>

---

## USAGE

Provided that you did not install other statusline plugins, `vim-moodo` should
work OOTB, unless you've `set laststatus=0`.

By default, `vim-moodo` will display the following information:

- current mode (normal, insert, visual, etc.)
- filename (truncated at 40 characters) with file-related indicators (RO,
  modified, etc.)
- filetype
- current row / total row
- current column

For customization and options, see `:h vim-moodo-customization` and `:h
vim-moodo-options`, respectively.

---

## OPTIONS

### `g:moodo_modes`

You can edit the indicator for the different modes of VIm/NVIm. For example, if
your system supports Nerd Fonts, then you may set
```vimscript
let g:moodo_modes = {
  \ 'n'      : '',
  \ 'no'     : '',
  \ 'v'      : '',
  \ 'V'      : '',
  \ "\<C-V>" : '',
  \ 's'      : '閭',
  \ 'S'      : '閭',
  \ "\<C-S>" : '閭',
  \ 'i'      : '',
  \ 'ic'     : '',
  \ 'ix'     : '',
  \ 'R'      : '',
  \ 'Rc'     : '',
  \ 'Rv'     : '',
  \ 'Rx'     : '',
  \ 'c'      : 'גּ',
  \ 'cv'     : 'גּ',
  \ 'ce'     : 'גּ',
  \ 'r'      : '',
  \ 'rm'     : '',
  \ '!'      : '',
  \ 't'      : ''
  \ }
```

### `g:moodo_colors`

vim-moodo uses custom highlight-groups for the different modes. If you simply wish to change the colors, I recommend that you change the values for the custom highlight groups.

The default for `g:moodo_colors` is as follows:
```vimscript
let g:moodo_colors = {
  \ 'n'      : '%#StatusLine#',
  \ 'no'     : '%#MoodoNormal#',
  \ 'v'      : '%#MoodoVisual#',
  \ 'V'      : '%#MoodoVisual#',
  \ "\<C-V>" : '%#MoodoVisual#',
  \ 's'      : '%#MoodoSelect#',
  \ 'S'      : '%#MoodoSelect#',
  \ "\<C-S>" : '%#MoodoSelect#',
  \ 'i'      : '%#MoodoInsert#',
  \ 'ic'     : '%#MoodoInsert#',
  \ 'ix'     : '%#MoodoInsert#',
  \ 'R'      : '%#MoodoReplace#',
  \ 'Rc'     : '%#MoodoReplace#',
  \ 'Rv'     : '%#MoodoReplace#',
  \ 'Rx'     : '%#MoodoReplace#',
  \ 'c'      : '%#MoodoCommand#',
  \ 'cv'     : '%#MoodoCommand#',
  \ 'ce'     : '%#MoodoCommand#',
  \ 'r'      : '%#MoodoMore#',
  \ 'rm'     : '%#MoodoMore#',
  \ '!'      : '%#MoodoCommand#',
  \ 't'      : '%#MoodoTerminal#'
  \ }
```

### `g:moodo_fticons`

By default, this variable is empty. It is useful for yourself to replace filetype names with icons and/or custom name for filetypes. For example, you may wish to set the startify filetype into start. To do so:
```vimscript
let g:moodo_fticons = {
  \ 'startify' : 'start'
  \ }
```
One may also use this option to use unicode to replace plain old filetypes.

Other options:
- `g:moodo_modified_indicator`
- `g:moodo_tabline_closestr`

---

## Tips

You may append custom indicators to the end of vim-moodo by way of defining your own function under the funcname `MoodoIndicators()`. For example, say if you want to show an indicator for when you have a async process that you toggle in VIm/NVim, and say it has a variable for your checking named `g:myprocess_isactive`. Then you may define
```vimscript
function! MoodoIndicators() abort
  let myprocess_indicator = g:myprocess_isactive ? ' P ' : ''
  let str = myprocess_indicator
  return str
endfunction
```
Then if `g:myprocess_isactive` returns true, the indicator 'P' will be shown appended to vim-moodo.
